# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%      | Y         |


---

### How to use 

| **Basic components** | **Description**  | **Use**            |              **Image**               |
| ---------------- | -------------------- | ------------------ |:-----------------------------------:|
| Brush            | 隨意揮灑心中的創意 |  長按拖曳   | ![](https://i.imgur.com/REOPZWD.png) |
| Eraser           | 任意塗改半途的創作 |  長按拖曳   | ![](https://i.imgur.com/5dkVu0i.png) |
| Color Selector   | 選擇內心渴望的顏色 |  點入選擇   | ![](https://i.imgur.com/hocFTzh.png) |
| Brush Size Slider| 調整筆刷需要的粗細 |  滑動調整   | ![](https://i.imgur.com/wpGWh9W.png) |
| Text Input       | 盡情插入想要的言語 |  點選輸入   | ![](https://i.imgur.com/3YtirVr.png) |
| Font Size        | 選擇文字需要的大小 |  列表選擇   | ![](https://i.imgur.com/7sskqZH.png) |
| Font Face        | 選擇文字需要的字體 |  列表選擇   | ![](https://i.imgur.com/OqgpWC7.png) |
| Cursor Icon      | 據所選工具改變樣貌 |  點選即可   |                                      |
| Refresh          | 重新開始腦中的靈感 |  點選即可   | ![](https://i.imgur.com/5SuBtxZ.png) |

 

| **Advanced tools** | **Description**    | **Use**    |              **Image**               |
| ------------------ | ------------------ | ---------- |:------------------------------------:|
| Circle             | 任意拖曳出想要的圓 | 長按拖曳    | ![](https://i.imgur.com/aNGVj3O.png) |
| Rectangle          | 隨心畫出喜歡的矩形 | 長按拖曳    | ![](https://i.imgur.com/9ap9YBc.png) |
| Triangle           | 繪畫標準的正三角形 | 長按拖曳    | ![](https://i.imgur.com/42PBc7C.png) |
| Un-do              | 一不小心失手的補救 | 點選即可    | ![](https://i.imgur.com/2jFNsQq.png) |
| Re-do              | 補救前者失手的補救 | 點選即可    | ![](https://i.imgur.com/QXZiho1.png) |
| Image Tool         | 上傳想要加工的圖片 | 點入選擇    | ![](https://i.imgur.com/OqdBpmQ.png) |
| Download           | 下載揮灑已久的作品 | 點選即可    | ![](https://i.imgur.com/WAYZs45.png) |

| **Other Useful Widgets (Bonus)** | **Description**    | **Use**    |              **Image**        |
| -------------------------------- | ------------------ | --- |:------------------------------------:|
| Normal Cursor                    | 回到鼠標最初的模樣 | 點選即可 | ![](https://i.imgur.com/8RAA4a2.png) |
| Line                             | 任意拖曳想要的線條 | 長按拖曳 | ![](https://i.imgur.com/XTewfNK.png) |
| Filled Shape                     | 是否填滿圓三角矩形 | 點選即可 | ![](https://i.imgur.com/DgLThei.png) |
| Color Wheel                      | 直覺快速的選擇顏色 | 點選即可 | ![](https://i.imgur.com/dv8xRoV.png) |
| Rainbow                          | 一下筆紅橙黃綠藍紫 | 點選即可 | ![](https://i.imgur.com/9XpsBO1.png) |
| BGM                              | 富有情調的繪圖氛圍 | 點按播放 | ![](https://i.imgur.com/X0ryiP1.png) |

                                   

### Gitlab page link

   [ https://107062111.gitlab.io/AS_01_WebCanvas](https://107062111.gitlab.io/AS_01_WebCanvas)

### Others 
お疲れ様~<3
[好看一點的md](https://hackmd.io/@2OmbLj7dQzClK-HMm84oqA/HJKw_dOPL)

<style>
table th{
    width: 100%;
}
</style>