window.addEventListener('load',function(){
    var canvas = document.getElementById('myCanvas');
    var ctx = canvas.getContext("2d");

    var sliderTrack = document.getElementById("slider_track");
    var slider = document.getElementById("slider");
   
    var upload = document.getElementById("up");
    var download = document.getElementById("down");
    var reset = document.getElementById("reset");   
    var cur = document.getElementById("cursor");
    var fill = document.getElementById("fill");
    var filled = false;
 
    var brush = document.getElementById("pen");
    var bru = false;
    var eraser = document.getElementById("era");
    var era = false;
    var text = document.getElementById("txt");
    var txt = false;
    var line = document.getElementById("line");
    var lin = false;
    var circle = document.getElementById("cir");
    var cir = false;
    var triangle = document.getElementById("tri");
    var tri = false;
    var rectangle = document.getElementById("rect");
    var rect = false;

    var undo = document.getElementById("undo");
    var redo = document.getElementById("redo");

    //color
    const myColor = document.getElementById("color");
    var color = myColor.value;    
    const rainbow = document.getElementById("rainbow");
    var rain = false;
    var changingColor = 0;

    const myFont = document.getElementById("fontface");
    var Font = myFont.value;
    const mySize = document.getElementById("fontsize");
    var Fontsize = mySize.value;

    var painting = document.getElementById('paint');
    var paint_style = getComputedStyle(painting);
    canvas.width = 810;
    canvas.height = 450;

    //add temporary canvas
    var canvast = document.getElementById("tmpCanvas");
    canvast.width = canvas.width;
    canvast.height = canvas.height;
    var ctxt = canvast.getContext('2d');

    var mouse = {x: 0, y: 0};
    var ori = {x:0, y:0};
    var move = {x:0, y:0};
    //prep slider
    var sliderX;
    var sliderLeft = $("#slider").position().left;
    var sliderLeftMin = sliderLeft;
    var sliderLeftMax = sliderLeft + $("#slider_track").width()*.965;
    var currentlySliding = false;

    var markerWidth = 2;
    ctx.lineWidth = markerWidth;

    function grab_slider(e) {
        sliderX = e.pageX;
        currentlySliding = true;                                    
    }
      
    function drag_slider(e) {
        if (currentlySliding) {
            var sliderXChange = e.pageX - sliderX;
            if (sliderLeft + sliderXChange < sliderLeftMin) {
            slider.style.left = sliderLeftMin + "px";
            } else if (sliderLeft + sliderXChange > sliderLeftMax) {
            slider.style.left = sliderLeftMax + "px";
            } else {
            slider.style.left = (sliderLeft + sliderXChange) + "px";
            };
        }
    }
    
    function drop_slider(e) {
        sliderLeft = $("#slider").position().left;
        updateMarker();
        currentlySliding = false;
    }
    
    function updateMarker() {
        //converts slider position to a proportional marker width of 2px-100px
        markerWidth = (sliderLeft-sliderLeftMin)*98/(sliderLeftMax-sliderLeftMin-sliderLeftMin)+2;
        ctx.lineWidth = markerWidth;
        ctxt.lineWidth = markerWidth;
    }
    
    function img_update(){
        ctxt.drawImage(canvas, 0, 0);
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }

    canvas.addEventListener('mousemove', function(e) {
        mouse.x = e.pageX - this.offsetLeft-10;
        mouse.y = e.pageY - this.offsetTop+10;
    }, false);
    
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    
    canvas.addEventListener('mousedown', function(e) {
        history.saveState(canvast);
        if(bru){
            ori.x = mouse.x;
            ori.y = mouse.y;
            canvas.addEventListener('mousemove', onPaint, false);
        }
        if(era){
            ctx.drawImage(canvast, 0, 0);
            ctxt.clearRect(0, 0, canvas.width, canvas.height);
            ctx.globalCompositeOperation = "destination-out";
            ctx.beginPath();
            ctx.moveTo(mouse.x, mouse.y);
            canvas.addEventListener('mousemove', onErase, false);
        }
        if(txt)onText();
        if(lin){
            ori.x = mouse.x;
            ori.y = mouse.y;
            canvas.addEventListener('mousemove', onLine, false);
        }
        if(cir){
            ori.x = mouse.x;
            ori.y = mouse.y;
            canvas.addEventListener('mousemove', onCir, false);
        }
        if(tri){
            ori.x = mouse.x;
            ori.y = mouse.y;
            canvas.addEventListener('mousemove', onTri, false);
        }
        if(rect){
            ctx.moveTo(mouse.x, mouse.y);
            ori.x = mouse.x;
            ori.y = mouse.y;
            canvas.addEventListener('mousemove',onRect, false);
        }
    }, false);
    
    canvas.addEventListener('mouseup', function() {
        img_update();
        if(bru)canvas.removeEventListener('mousemove', onPaint, false);
        if(era){
            ctx.globalCompositeOperation = "source-over";
            canvas.removeEventListener('mousemove', onErase, false);
        }
        if(lin)canvas.removeEventListener('mousemove', onLine, false);
        if(cir)canvas.removeEventListener('mousemove', onCir, false);
        if(tri)canvas.removeEventListener('mousemove', onTri, false);
        if(rect)canvas.removeEventListener('mousemove', onRect, false);
    }, false);
    
    var onPaint = function() {
        console.log("paint");
        if(changingColor===361)changingColor=0;
        if(rain) color = `hsl(${changingColor}, 100%, 50%)`;
        ctx.strokeStyle = color;
        ctx.beginPath();
        ctx.moveTo(ori.x, ori.y);
        ctx.lineTo(mouse.x, mouse.y);
        ctx.stroke();
        ori.x = mouse.x;
        ori.y = mouse.y;
        changingColor++;
    };

    var onErase = function(){
        console.log("erase");
        ctx.lineTo(mouse.x, mouse.y);
        ctx.stroke();
    }

    var onText = function(){
        let t = prompt('Text:', '');
        if(t){
            console.log(Font);
            ctxt.font = Fontsize + 'px ' + Font;
            ctxt.fillStyle = color;
            ctxt.fillText(t, mouse.x, mouse.y);
        }
    }

    var onLine = function(){
        console.log("line");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        if(changingColor===361)changingColor=0;
        if(rain) color = `hsl(${changingColor}, 100%, 50%)`;
        ctx.strokeStyle = color;
        ctx.beginPath();
        ctx.moveTo(ori.x, ori.y);
        ctx.lineTo(mouse.x, mouse.y);
        ctx.stroke();
        changingColor++;
    }

    var onCir = function(){
        console.log("circle");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        if(changingColor===361)changingColor=0;
        if(rain) color = `hsl(${changingColor}, 100%, 50%)`;
        ctx.beginPath();
        ctx.arc(ori.x, ori.y, Math.sqrt(Math.pow(mouse.x-ori.x, 2)+Math.pow(mouse.y-ori.y,2)), 0, 2*Math.PI);
        if(!filled){
            ctx.strokeStyle = color;
            ctx.stroke();            
        }
        else{
            ctx.fillStyle = color;
            ctx.fill();
        }
        changingColor++;
    }

    var onTri = function(){
        console.log("triangle");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        if(changingColor===361)changingColor=0;
        if(rain) color = `hsl(${changingColor}, 100%, 50%)`;
        ctx.beginPath();
        ctx.moveTo(ori.x, ori.y);
        ctx.lineTo(mouse.x, mouse.y);
        ctx.lineTo(2*ori.x-mouse.x, mouse.y);
        ctx.closePath();
        if(!filled){
            ctx.strokeStyle = color;
            ctx.stroke();            
        }
        else{
            ctx.fillStyle = color;
            ctx.fill();
        }
        changingColor++;
    }

    var onRect = function(){
        console.log("rectangle");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        if(changingColor===361)changingColor=0;
        if(rain) color = `hsl(${changingColor}, 100%, 50%)`;
        ctx.beginPath();
        ctx.rect(ori.x, ori.y, mouse.x-ori.x, mouse.y-ori.y);
        if(!filled){
            ctx.strokeStyle = color;
            ctx.stroke();            
        }
        else{
            ctx.fillStyle = color;
            ctx.fill();
        }
        changingColor++;
    }

    slider.addEventListener("mousedown", grab_slider);
    document.addEventListener("mousemove", drag_slider);
    document.addEventListener("mouseup", drop_slider);

    var history = {
        redo_list: [],
        undo_list: [],
        saveState: function(canvas, list, keep_redo) {
          keep_redo = keep_redo || false;
          if(!keep_redo) this.redo_list = [];
          
          (list || this.undo_list).push(canvas.toDataURL());   
        },
        undo: function(canvas, ctx) {
          this.restoreState(canvas, ctx, this.undo_list, this.redo_list);
        },
        redo: function(canvas, ctx) {
          this.restoreState(canvas, ctx, this.redo_list, this.undo_list);
        },
        restoreState: function(canvas, ctx,  pop, push) {
            if(pop.length) {
                this.saveState(canvas, push, true);
                var restore_state = pop.pop();
                var img = new Image(canvas.width, canvas.height);
                img.src = restore_state;
                img.onload = function(){
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    ctxt.clearRect(0, 0, canvas.width, canvas.height);
                    ctxt.drawImage(img, 0, 0);                   
                }
            }
        }
      }

    myFont.onchange = function(){
        Font = myFont.value;
    }

    myColor.onclick= function(){
        color = myColor.value;
        rain = false;
    }

    myColor.onchange= function(){
        color = myColor.value;
        rain = false;
    }

    rainbow.onclick = function(){
        rain = true;
    }

    mySize.onchange = function(){
        Fontsize = mySize.value;
    }

    brush.onclick = function(){
        document.body.style.cursor="url('cur/brush.png'),auto";
        bru = true;
        era = false;
        txt = false;
        lin = false;
        cir = false;
        tri = false;
        rect = false;
    }

    eraser.onclick = function(){
        document.body.style.cursor="url('cur/eraser.png'),auto";
        bru = false;
        era = true;
        txt = false;
        lin = false;
        cir = false;
        tri = false;
        rect = false;
    }

    text.onclick = function(){
        document.body.style.cursor="url('cur/text.png'),auto";
        bru = false;
        era = false;
        txt = true;
        lin = false;
        cir = false;
        tri = false;
        rect = false;
    }

    line.onclick = function(){
        document.body.style.cursor="url('cur/line.png'),auto";
        bru = false;
        era = false;
        txt = false;
        lin = true;
        cir = false;
        tri = false;
        rect = false;       
    }

    circle.onclick = function(){
        document.body.style.cursor="url('cur/circle.png'),auto";
        bru = false;
        era = false;
        txt = false;
        lin = false;
        cir = true;
        tri = false;
        rect = false;
    }

    triangle.onclick = function(){
        document.body.style.cursor="url('cur/triangle.png'),auto";
        bru = false;
        era = false;
        txt = false;
        lin = false;
        cir = false;
        tri = true;
        rect = false;
    }

    rectangle.onclick = function(){
        document.body.style.cursor="url('cur/rectangle.png'),auto";
        bru = false;
        era = false;
        txt = false;
        lin = false;
        cir = false;
        tri = false;
        rect = true;
    }

    upload.onchange = function() {
        let reader = new FileReader();
        let img = new Image();
        reader.onloadend = function() {
            img.src = reader.result;
        }
        reader.readAsDataURL(this.files[0]);
        img.onload = function() {
            ctxt.clearRect(0, 0, canvas.width, canvas.height);
            ctxt.drawImage(img, 0, 0);
        }
        history.saveState(canvast);
    } 

    reset.onclick= function(){
        history.saveState(canvast);
        ctxt.clearRect(0,0, canvas.width, canvas.height);
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
    
    download.onclick= function() {
        download.href = canvast.toDataURL();
        download.download = "mypainting.png";
    }
    
    cur.onclick = function(){
        document.body.style.cursor="auto";
        bru = false;
        era = false;
        txt = false;
        lin = false;
        cir = false;
        tri = false;
        rect = false;
    }

    fill.onclick = function(){
        filled = !filled
    }
    
    undo.onclick = function(){
        console.log("undo");
        history.undo(canvast, ctxt);
    }

    redo.onclick = function(){
        console.log("redo");
        history.redo(canvast, ctxt);
    }

    function selectcolor(){
        color = this.style.backgroundColor;
        rain = false;
    }

    var colorw = [];

    for (var i=0; i<360; i++) {
        colorw[i] = document.createElement("span");
        colorw[i].setAttribute("id", "d" + i);
        colorw[i].style.backgroundColor = "hsl(" + i + ", 100%, 50%)";
        colorw[i].style.msTransform = "rotate(" + i + "deg)";
        colorw[i].style.webkitTransform = "rotate(" + i + "deg)";
        colorw[i].style.MozTransform = "rotate(" + i + "deg)";
        colorw[i].style.OTransform = "rotate(" + i + "deg)";
        colorw[i].style.transform = "rotate(" + i + "deg)";
        document.getElementById('colorwheel').appendChild(colorw[i]);
        colorw[i].onclick = selectcolor;
    };

}, false)


